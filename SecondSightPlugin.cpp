// Example low level rendering Unity plugin
#include "SecondSightPlugin.h"
#include "Unity/IUnityGraphics.h"

#include <math.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <boost/thread.hpp>
#include <opencv2/opencv.hpp>
#include <nanomsg/nn.h>
#include <nanomsg/pubsub.h>
#include <queue>

#include <libsecondsight/libsecondsight.h>

using namespace libsecondsight;

class UnityCommandServer
{
public:
  UnityCommandServer(std::string url, std::string url_boxes)
  {
    m_bounding_boxes_url = url_boxes;
    m_url = url;
    m_isConnected = false;
    m_shouldExit = false;
    m_refCount = 0;
    m_hasExited = false;
    boost::thread t0(WriteCommand, this);
    boost::thread t1(ReadBoundingBoxes, this);
  }
  
  ~UnityCommandServer()
  {
  }
  
  static void WriteCommand(UnityCommandServer *server)
  {
    int sock = nn_socket (AF_SP, NN_PUB);
    bool success = (sock >= 0);
    success |= (nn_bind (sock, server->m_url.c_str()) >= 0);
    assert(success);
    
    server->m_socket = sock;
    while (!server->m_shouldExit)
    {
      
      server->m_lock.lock();
      if(!server->m_commandQueue.empty())
      {
        UnityCommandEnum curCommand = server->m_commandQueue.front();
        server->m_commandQueue.pop();
        SerializedUnityCommand command;
        SerializeCommand(&command, curCommand);
        
        std::string msg;
        command.SerializeToString(&msg);
        nn_send(sock, msg.c_str(), msg.size(), 0);
      }
      server->m_lock.unlock();
    }
    
    server->m_isConnected = false;
    nn_shutdown(sock, 0);
    nn_close(sock);
    server->m_hasExited = true;
  }
  
  static void ReadBoundingBoxes(UnityCommandServer *server)
  {

    int sock = nn_socket (AF_SP, NN_SUB);
    bool success = (sock >= 0);
    assert(success);
    success = (nn_setsockopt(sock, NN_SUB, NN_SUB_SUBSCRIBE, "", 0) >= 0);
    assert(success);
    success = (nn_connect(sock, server->m_bounding_boxes_url.c_str()) >= 0);
    assert(success);
    
    while (!server->m_shouldExit)
    {
      char *buf = NULL;
      int numBytes = nn_recv (sock, &buf, NN_MSG, 0);
      assert (numBytes >= 0);
      
      SerializedBoundingBoxList list;
      list.ParseFromArray(buf, numBytes);
      
      server->m_bounding_box_lock.lock();
      server->m_bounding_boxes.clear();
      for(int i = 0; i < list.boxes_size(); i++) {
        AABox box;
        DeserializeAABox(&list.boxes(i), box);
        server->m_bounding_boxes.push_back(box);
      }
      server->m_bounding_box_lock.unlock();
      
      //Free the buffer
      nn_freemsg (buf);
    }
    
    nn_shutdown(sock, 0);
    nn_close(sock);
  }
  
  std::vector<AABox>                m_bounding_boxes;
  boost::mutex                      m_bounding_box_lock;
  std::string                       m_url;
  std::string                       m_bounding_boxes_url;
  int                               m_socket;
  bool                              m_isConnected;
  bool                              m_shouldExit;
  std::queue<UnityCommandEnum>      m_commandQueue;
  boost::mutex                      m_lock;
  int                               m_refCount;
  bool                              m_hasExited;
};

// --------------------------------------------------------------------------
// Global variables
typedef std::shared_ptr<UnityCommandServer> UnityCommandServerPtr;
UnityCommandServerPtr  g_server = nullptr;

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API Initialize()
{
  if(g_server != nullptr && g_server->m_hasExited)
  {
    g_server = nullptr;
  }
  
  if(g_server == nullptr)
  {
    g_server = UnityCommandServerPtr(new UnityCommandServer(g_unityCommandServerURL, g_unityBoundingBoxURL));
  }
  
  g_server->m_refCount++;
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetCommand(int command)
{
  if(g_server == nullptr || g_server->m_hasExited)
    return;
  
  g_server->m_lock.lock();
  g_server->m_commandQueue.push((UnityCommandEnum)command);
  g_server->m_lock.unlock();
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API Shutdown()
{
  if(g_server != nullptr && g_server->m_hasExited)
  {
    g_server = nullptr;
  }
  
  if(g_server != nullptr)
  {
    g_server->m_refCount--;
    if(g_server->m_refCount <= 0)
    {
      g_server->m_shouldExit = true;
    }
  }
}

extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetNumberOfBoundingBoxes()
{
  if(g_server == nullptr || g_server->m_hasExited)
    return 0;
  
  g_server->m_bounding_box_lock.lock();
  int numBoxes = g_server->m_bounding_boxes.size();
  g_server->m_bounding_box_lock.unlock();
  return numBoxes;
}

extern "C" AABox UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetBoundingBox(int index)
{
  AABox box;
  box.xMin = 0; box.xMax = 0; box.yMin = 0; box.yMax = 0; box.zMin = 0; box.zMax = 0;
  if(g_server == nullptr || g_server->m_hasExited)
    return box;
  
  g_server->m_bounding_box_lock.lock();
  if(index < 0 || index >= g_server->m_bounding_boxes.size())
  {
    g_server->m_bounding_box_lock.unlock();
    return box;
  }
  box = g_server->m_bounding_boxes[index];
  g_server->m_bounding_box_lock.unlock();
  return box;
}
